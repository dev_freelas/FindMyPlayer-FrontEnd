import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelecionarHeroiModalPage } from './selecionar-heroi-modal';

@NgModule({
  declarations: [
    SelecionarHeroiModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SelecionarHeroiModalPage),
  ],
})
export class SelecionarHeroiModalPageModule {}
