import { GlobalVariable } from './../../global';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Relacionamento } from "../../models/Relacionamento";
import { Observable } from "rxjs/Observable";

/*
  Generated class for the RelacionamentoServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class RelacionamentoServiceProvider {

  constructor(public http: Http) {
    console.log('Hello RelacionamentoServiceProvider Provider');
  }
  getRelacionamentos(): Observable<Relacionamento[]> {
    return this.http.get(GlobalVariable.BASE_API + "Itens")
      .map(data => data.json() as Relacionamento[])
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getRelacionamento(id): Observable<Relacionamento> {
    return this.http.get(GlobalVariable.BASE_API + "Itens/" + id)
      .map(response => response.json().data as Relacionamento)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  addRelacionamento(relacionamento: Relacionamento): Observable<Relacionamento>{
      let rel = JSON.stringify(relacionamento, [
        'usuario1Id',
        'tipoRelacionamentoId',
        'usuario2Id',
      ]);
      let header = new Headers({
        'Content-Type': 'application/json'
      });
      return this.http.post(GlobalVariable.BASE_API + "Relacionamentos", rel, { headers: header })
        .map(response => {
          return response.json() as Relacionamento;
        })
        .catch((error: any) => Observable.throw(error || 'Server error'));
  }
}
