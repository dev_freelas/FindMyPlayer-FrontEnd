import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { GlobalVariable } from './../../global';
import { Heroi } from './../../models/Heroi';

/*
  Generated class for the HeroisServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class HeroisServiceProvider {

  constructor(public http: Http) {
    console.log('Hello HeroisServiceProvider Provider');
  }

  getHeroi(id: number): Observable<Heroi> {
    return this.http.get(GlobalVariable.BASE_API + "Herois/"+id)
      .map(data => data.json() as Heroi)
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getHerois(): Observable<Heroi[]> {
    return this.http.get(GlobalVariable.BASE_API + "Herois")
      .map(data => data.json() as Heroi[])
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}
