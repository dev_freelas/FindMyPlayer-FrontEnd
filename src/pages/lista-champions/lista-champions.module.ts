import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaChampionsPage } from './lista-champions';

@NgModule({
  declarations: [
    ListaChampionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaChampionsPage),
  ],
})
export class ListaChampionsPageModule {}
