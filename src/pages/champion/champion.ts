import { ChampionCountersPage } from './champion-counters/champion-counters';
import {
  ChampionUserbuildsPage
} from './../champion/champion-userbuilds/champion-userbuilds';
import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
} from 'ionic-angular';
import {
  ChampionInfoPage
} from "./champion-info/champion-info";
import {
  ChampionBuildPage
} from "./champion-build/champion-build";

import { Heroi } from "../../models/Heroi";

/**
 * Generated class for the ChampionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-champion',
  templateUrl: 'champion.html',
})
export class ChampionPage {

  tabInfo: any;
  tabCounters: any;
  tabUserBuild: any;
  tabBuild: any;

  heroi: Heroi;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
   
    // this.heroi = navParams.get("heroi");
    this.heroi = navParams.data as Heroi;
    
    this.tabInfo = ChampionInfoPage;
    this.tabCounters = ChampionCountersPage;
    this.tabUserBuild = ChampionUserbuildsPage;
    this.tabBuild = ChampionBuildPage;
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChampionPage');
  }

}