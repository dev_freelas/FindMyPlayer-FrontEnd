import { ListaChampionsPage } from './../pages/lista-champions/lista-champions';
import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule} from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';

import { MeusMatchesPage } from './../pages/match-main/meus-matches/meus-matches';
import { MatchMainPage } from './../pages/match-main/match-main';
import { MatchPage } from './../pages/match-main/match/match';
import { IonicStorageModule } from '@ionic/storage';
import { LanesImagesPipe } from './../pipes/lanes-images/lanes-images';
import { MainPage } from './../pages/main/main';
import { LolImagensPipe } from './../pipes/lol-imagens/lol-imagens';
import { SelecionarHeroiModalPage } from './../pages/selecionar-heroi-modal/selecionar-heroi-modal';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from './../pages/register/register';
import { WelcomePage } from './../pages/welcome/welcome';
import { LolAccountModalPage } from './../pages/lol-account-modal/lol-account-modal';
import { TelaIntegracaoPage } from './../pages/tela-integracao/tela-integracao';
import { JogoServiceProvider } from '../providers/jogo-service/jogo-service';
import { HeroisServiceProvider } from '../providers/herois-service/herois-service';
import { ItemServiceProvider } from '../providers/item-service/item-service';
import { UsuarioServiceProvider } from '../providers/usuario-service/usuario-service';
import { LeagueoflegendsServiceProvider } from '../providers/leagueoflegends-service/leagueoflegends-service';
import { LaneServiceProvider } from '../providers/lane-service/lane-service';
import { LeagueoflegendsRegioesServiceProvider } from '../providers/leagueoflegends-regioes-service/leagueoflegends-regioes-service';
import { RelacionamentoServiceProvider } from '../providers/relacionamento-service/relacionamento-service';
import { TipoRelacionamentoServiceProvider } from '../providers/tipo-relacionamento-service/tipo-relacionamento-service';
import { ChampionPage} from "../pages/champion/champion";
import { ChampionInfoPage} from "../pages/champion/champion-info/champion-info";
import { ChampionBuildPage} from "../pages/champion/champion-build/champion-build";
import { ChampionUserbuildsPage } from './../pages/champion/champion-userbuilds/champion-userbuilds';
import { ChampionCountersPage } from './../pages/champion/champion-counters/champion-counters';
import { SelecionarItemModalPage} from "../pages/selecionar-item-modal/selecionar-item-modal";
import { BuildServiceProvider } from '../providers/build-service/build-service';
import { BuildItemServiceProvider } from '../providers/build-item-service/build-item-service';
import { CountersServiceProvider } from '../providers/counters-service/counters-service';
import { DicasServiceProvider } from '../providers/dicas-service/dicas-service';
import { ProjetosProvider } from '../providers/projetos/projetos';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    WelcomePage,
    RegisterPage,
    TelaIntegracaoPage,
    LolAccountModalPage,
    SelecionarHeroiModalPage,
    SelecionarItemModalPage,
    ChampionPage,
    ChampionBuildPage,
    ChampionInfoPage,
    ChampionUserbuildsPage,
    MainPage,
    MatchPage,
    MatchMainPage,
    MeusMatchesPage,
    ChampionCountersPage,
    ListaChampionsPage,
    LolImagensPipe,
    LanesImagesPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    WelcomePage,
    RegisterPage,
    TelaIntegracaoPage,
    LolAccountModalPage,
    SelecionarHeroiModalPage,
    SelecionarItemModalPage,
    ChampionPage,
    ChampionInfoPage,
    ChampionBuildPage,
    ChampionUserbuildsPage,
    MainPage,
    MatchPage,
    MatchMainPage,
    MeusMatchesPage,
    ChampionCountersPage,
    ListaChampionsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    JogoServiceProvider,
    HeroisServiceProvider,
    ItemServiceProvider,
    UsuarioServiceProvider,
    LeagueoflegendsServiceProvider,
    LaneServiceProvider,
    LeagueoflegendsRegioesServiceProvider,
    RelacionamentoServiceProvider,
    TipoRelacionamentoServiceProvider,
    BuildServiceProvider,
    BuildItemServiceProvider,
    BuildServiceProvider,
    CountersServiceProvider,
    Storage,
    DicasServiceProvider,
    ProjetosProvider
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
