import { BaseModel } from './BaseModel';
export class TipoRelacionamento extends BaseModel{
     TipoRelacionamentoId: number;
     Nome: string;
}