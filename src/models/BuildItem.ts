import { BaseModel } from './BaseModel';
import { Build } from './Build';
import { Item } from './Item';
export class BuildItem extends BaseModel{
     buildItemId: number;
     itemId?: number;
     item?: Item;
     buildId?: number;
     build?: Build;
     descricao?: string;
}