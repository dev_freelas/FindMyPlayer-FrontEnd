import { BaseModel } from './BaseModel';
import { Usuario } from './Usuario';
import { Heroi } from './Heroi';
export class Dica extends BaseModel{
     dicaId?: number;
     descricao?: number;
     heroiId?: number;
     heroi?: Heroi;
     usuarioId?: number;
     usuario?: Usuario;
}