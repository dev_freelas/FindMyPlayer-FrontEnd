import { GlobalVariable } from './../../global';
import { Observable } from 'rxjs/Rx';
import { BuildItem } from './../../models/BuildItem';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the BuildItemServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BuildItemServiceProvider {

  constructor(public http: Http) {
    console.log('Hello BuildItemServiceProvider Provider');
  }

  getBuildItem(id: number): Observable<BuildItem> {
    return this.http.get(GlobalVariable.BASE_API + "BuildItens/" + id)
      .map(res => res.json() as BuildItem)
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getBuildItems(): Observable<BuildItem[]> {
    return this.http.get(GlobalVariable.BASE_API + "BuildItens")
      .map(res => res.json() as BuildItem[])
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  addBuild(build: BuildItem): Observable<BuildItem> {
    let item = JSON.stringify(build, [
      'itemId',
      'buildId'
    ]);
    let header = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(GlobalVariable.BASE_API + "BuildItens", item, { headers: header })
      .map(response => {
        return response.json() as BuildItem;
      })
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}
