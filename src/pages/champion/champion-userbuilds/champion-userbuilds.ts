import { Item } from './../../../models/Item';
import { BuildItem } from './../../../models/BuildItem';
import { BuildItemServiceProvider } from './../../../providers/build-item-service/build-item-service';
import { ItemServiceProvider } from './../../../providers/item-service/item-service';
import { BuildServiceProvider } from './../../../providers/build-service/build-service';
import { Build } from './../../../models/Build';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Heroi } from "../../../models/Heroi";

/**
 * Generated class for the ChampionUserbuildsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-champion-userbuilds',
  templateUrl: 'champion-userbuilds.html',
})
export class ChampionUserbuildsPage {
  heroi: Heroi;
  builds: Build[];
  buildItems: BuildItem[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private apiBuild: BuildServiceProvider,
    private apiItem: ItemServiceProvider,
    private apiBuiltItem: BuildItemServiceProvider) {
    console.log('Passed params', navParams.data);
    this.heroi = navParams.data as Heroi;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChampionUserbuildsPage');
    this.apiBuild.getBuilds().subscribe(
      (resp: Build[]) => this.builds = resp,
      err => console.log(err),
      () => this.recuperarBuildItems()
    );
  }


  recuperarBuildItems() {
    this.apiBuiltItem.getBuildItems().subscribe(
      res => this.builds
        .forEach(element => {
          element.listaItemsId = res.filter(a => a.buildId == element.buildId).map(b => b.itemId);
        }),
      err => console.log('erro ao recuperar builditems: ' + err),
      () => this.recuperarItems()
    )

  }

  recuperarItems() {
    this.builds.forEach((element, index) => {
      element.listaItemsId.forEach(itemid => {
        this.requestItem(index, itemid);
      });
    });
  }

  requestItem(index, itemid) {
    this.apiItem.getItem(itemid).subscribe(
      res => {
        if(!this.builds[index].listaItems)
          this.builds[index].listaItems = [];
        this.builds[index].listaItems.push(res);
      },
      err => console.log('erro ao recuperar item: ' + err),
      () => console.log(this.builds)
    );
  }

}
