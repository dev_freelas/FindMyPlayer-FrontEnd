import { BaseModel } from './BaseModel';
import { Jogo } from './Jogo';
import { Dica } from "./Dica";
export class Heroi extends BaseModel{
    heroiId: number;
    nome?: string;
    historia?: string;
    titulo?: string;
    imagem?: string;
    jogoId?: number;
    championId?: number;
    jogo?: Jogo;
    dicas?: Dica[];
}
