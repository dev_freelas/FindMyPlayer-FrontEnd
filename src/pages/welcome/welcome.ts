import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ViewController, Nav } from 'ionic-angular';

import { UsuarioServiceProvider } from './../../providers/usuario-service/usuario-service';
import { HomePage } from './../home/home';
import { RegisterPage } from './../register/register';
/**
 * Generated class for the WelcomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  @ViewChild(Nav) nav: Nav;
  

  usuario: string;
  senha: string;

  constructor(public navCtrl: NavController,
    private viewCtrl: ViewController,
    public navParams: NavParams,
    private http: Http,
    private apiUsuario: UsuarioServiceProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

  login() {

    if (this.usuario && this.senha) {
      let loading = this.loadingCtrl.create({ content: 'Please Wait...' });
      loading.present();
      this.apiUsuario.login(this.usuario, this.senha).subscribe(
        resp => {
          if (resp && resp.status == 'OK') {
            loading.dismiss();
            this.navCtrl.popToRoot();
            this.navCtrl.setRoot(HomePage);
          }
          else {
            let alert = this.alertCtrl.create({
              title: 'Fail to login',
              message: resp ? (resp as any).mensagem : "",
              buttons: ['OK']
            });
            loading.dismiss();
            alert.present();
          }
        },
        err => {
          let alert = this.alertCtrl.create({
            title: 'Fail to login',
            message: err,
            buttons: ['OK']
          });
          loading.dismiss();
          alert.present();
        }
      );

    }
  }

  register() {
    this.navCtrl.push(RegisterPage);
  }

}
