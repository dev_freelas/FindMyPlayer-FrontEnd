import { BaseModel } from './BaseModel';
import { Build } from './Build';
import { Usuario } from './Usuario';
export class Comentario extends BaseModel{
     ComentarioId: number;
     Descricao: number;
     UsuarioId: number;
     Usuario: Usuario;
     BuildId: number;
     Build: Build;
}