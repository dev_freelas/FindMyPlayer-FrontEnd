import { Item } from './Item';
import { Comentario } from './Comentario';
import { Usuario } from "./Usuario";
import { BaseModel } from "./BaseModel";

export class Build extends BaseModel{
    
    buildId: number;
    nome: string;
    descricao: string;
    usuarioId: number;
    heroiId: number;
    comentarios: Comentario[];

    listaItemsId?: number[];
    listaItems: Item[] = [];
}