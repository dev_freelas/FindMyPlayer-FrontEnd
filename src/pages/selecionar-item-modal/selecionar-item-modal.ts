import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { Item } from './../../models/Item';
import { ItemServiceProvider } from './../../providers/item-service/item-service';

/**
 * Generated class for the SelecionarItemModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selecionar-item-modal',
  templateUrl: 'selecionar-item-modal.html',
})
export class SelecionarItemModalPage {

  items: Item[];
  numero: number;

  constructor(private viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiItem: ItemServiceProvider) {
    this.numero = navParams.get('numero');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelecionaritemModalPage');
    this.apiItem.getItems().subscribe(
      data => {
        this.items = data;
      },
      erro => console.log(erro)
    );
  }

  onClickSelectItem(evento, item) {
    this.viewCtrl.dismiss({ numero: this.numero, item: item });
  }

}
