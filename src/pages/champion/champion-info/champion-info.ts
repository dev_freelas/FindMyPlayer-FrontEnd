import { DicasServiceProvider } from './../../../providers/dicas-service/dicas-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Heroi } from "../../../models/Heroi";

/**
 * Generated class for the ChampionInfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-champion-info',
  templateUrl: 'champion-info.html',
})
export class ChampionInfoPage {

  champion: Heroi;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private apiDicas: DicasServiceProvider) {
    console.log('Passed params', navParams.data);
    this.champion = navParams.data as Heroi;    
    this.champion.historia.replace('<br>', '\n');
    
    // let heroi = navParams.get('heroi');
    // if (heroi){
    //   this.champion = navParams.get('heroi');
    // }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChampionInfoPage');
    this.apiDicas.getDicas().subscribe(
      res => this.champion.dicas = res,
      err => console.log(err)
    );
  }

}
