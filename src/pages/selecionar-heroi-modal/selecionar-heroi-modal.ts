import { Heroi } from './../../models/Heroi';
import { HeroisServiceProvider } from './../../providers/herois-service/herois-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the SelecionarHeroiModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selecionar-heroi-modal',
  templateUrl: 'selecionar-heroi-modal.html',
})
export class SelecionarHeroiModalPage {

  herois: Heroi[];

  constructor(private viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiHeroi: HeroisServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelecionarHeroiModalPage');
    this.apiHeroi.getHerois().subscribe(
      (data: Heroi[]) => this.herois = data,
      erro => console.log(erro));
  }

  onClickSelectHeroi(evento, heroi) {
    this.viewCtrl.dismiss(heroi);
  }
  
}
