import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the LanesImagesPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'lanesImages',
})
export class LanesImagesPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: number, ...args) {
    switch (value){
      case 1:
        return `assets/icon/lanes-icon/top_icon.png`
      case 2:
        return `assets/icon/lanes-icon/mid_icon.png`
      case 3:
        return `assets/icon/lanes-icon/bot_icon.png`
      case 4:
        return `assets/icon/lanes-icon/jungle_icon.png`
    }
  }
}
