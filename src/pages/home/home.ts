import { ListaChampionsPage } from './../lista-champions/lista-champions';
import { MatchMainPage } from './../match-main/match-main';
import { WelcomePage } from './../welcome/welcome';
import { UsuarioServiceProvider } from './../../providers/usuario-service/usuario-service';
import { MainPage } from './../main/main';
import { ChampionPage } from './../champion/champion';
import { Component, ViewChild } from '@angular/core';
import { NavController, Nav, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild(Nav) nav: Nav;

  pages: Array<{ title: string, component: any }>;
  rootPage: MatchMainPage;

  constructor(public navCtrl: NavController,
    private viewCtrl: ViewController,
    private apiUsuario: UsuarioServiceProvider) {
    this.pages = [
      // { title: 'Main', component: MainPage },
      { title: 'Match', component: MatchMainPage },
      { title: 'Champion', component: ListaChampionsPage },
    ];
  }

  ionViewDidLoad() {
    this.openPage(this.pages[0]);
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  logout() {
    this.apiUsuario.logout();
    this.navCtrl.setRoot(WelcomePage);
    this.navCtrl.popToRoot();
  }

}
