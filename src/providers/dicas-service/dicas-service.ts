import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { GlobalVariable } from './../../global';
import { Dica } from './../../models/Dica';
/*
  Generated class for the DicasServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DicasServiceProvider {

  constructor(public http: Http) {
    console.log('Hello DicasServiceProvider Provider');
  }

  getDicas(): Observable<Dica[]> {
    return this.http.get(GlobalVariable.BASE_API + "Dicas")
      .map(data => data.json() as Dica[])
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getDica(id): Observable<Dica> {
    return this.http.get(GlobalVariable.BASE_API + "Dicas/" + id)
      .map(response => response.json().data as Dica)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  addDica(Dica: Dica): Observable<Dica>{
      let rel = JSON.stringify(Dica, [
        'descricao',
        'heroiId',
        'usuarioId',
      ]);
      let header = new Headers({
        'Content-Type': 'application/json'
      });
      return this.http.post(GlobalVariable.BASE_API + "Dicas", rel, { headers: header })
        .map(response => {
          return response.json() as Dica;
        })
        .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}
