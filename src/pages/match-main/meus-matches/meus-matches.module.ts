import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeusMatchesPage } from './meus-matches';

@NgModule({
  declarations: [
    MeusMatchesPage,
  ],
  imports: [
    IonicPageModule.forChild(MeusMatchesPage),
  ],
})
export class MeusMatchesPageModule {}
