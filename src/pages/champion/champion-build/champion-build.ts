import { BuildItemServiceProvider } from './../../../providers/build-item-service/build-item-service';
import { BuildServiceProvider } from './../../../providers/build-service/build-service';
import { BuildItem } from './../../../models/BuildItem';
import { UsuarioServiceProvider } from './../../../providers/usuario-service/usuario-service';
import { Usuario } from './../../../models/Usuario';
import { Build } from './../../../models/Build';
import { Heroi } from './../../../models/Heroi';
import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Item } from "../../../models/Item";
import { SelecionarItemModalPage } from "../../selecionar-item-modal/selecionar-item-modal";

/**
 * Generated class for the ChampionBuildPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-champion-build',
  templateUrl: 'champion-build.html',
})
export class ChampionBuildPage {
  heroi: Heroi;
  build: Build;
  item1: Item;
  item2: Item;
  item3: Item;
  item4: Item;
  item5: Item;
  descricaoItem1: string;
  descricaoItem2: string;
  descricaoItem3: string;
  descricaoItem4: string;
  descricaoItem5: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private usuarioService: UsuarioServiceProvider,
    private apiBuild: BuildServiceProvider,
    private apiBuildItems: BuildItemServiceProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {
    console.log('Passed params', navParams.data);
    this.heroi = navParams.data as Heroi;
    this.build = new Build;
    this.build.heroiId = this.heroi.heroiId;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChampionBuildPage');
  }

  clickEscolhaItem(item: number) {
    let modal = this.modalCtrl.create(SelecionarItemModalPage, { numero: item });
    modal.present();
    modal.onDidDismiss(
      (data: any) => {
        switch (data.numero) {
          case 1:
            this.item1 = data.item;
            break;
          case 2:
            this.item2 = data.item;
            break;
          case 3:
            this.item3 = data.item;
            break;
          case 4:
            this.item4 = data.item;
            break;
          case 5:
            this.item5 = data.item;
            break;
        }

      }
    );
  }

  onClickSave(event) {
    let loading = this.loadingCtrl.create({ content: 'Please Wait...' });
    loading.present();

    let usuario = this.usuarioService.getUsuarioLogado().usuarioId;
    this.build.usuarioId = usuario;
    this.apiBuild.addBuild(this.build).subscribe(
      resp => {
        this.build = resp;
        if (this.item1 &&
          this.item2 &&
          this.item3 &&
          this.item4 &&
          this.item5
        ) {
          let itens: BuildItem[] = [
            { itemId: this.item1.itemId, buildId: this.build.buildId, descricao: this.descricaoItem1 } as BuildItem,
            { itemId: this.item2.itemId, buildId: this.build.buildId, descricao: this.descricaoItem2 } as BuildItem,
            { itemId: this.item3.itemId, buildId: this.build.buildId, descricao: this.descricaoItem3 } as BuildItem,
            { itemId: this.item4.itemId, buildId: this.build.buildId, descricao: this.descricaoItem4 } as BuildItem,
            { itemId: this.item5.itemId, buildId: this.build.buildId, descricao: this.descricaoItem5 } as BuildItem,
          ];
          itens.forEach(element => {
            this.apiBuildItems.addBuild(element).subscribe(
              resp => {
                console.log("adicionado: " + resp)
              },
              err => console.log("erro: " + err)
            );
          });
        }
      },
      erro => alert(erro),
      () => {
        loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Completed',
            buttons: ['OK']
          });
        alert.present();
        this.limparCampos();
      }
    );

  }

  limparCampos() {
    this.build = new Build;
    this.build.heroiId = this.heroi.heroiId;
    this.build.nome = '';
    this.build.descricao = '';
    
    this.item1 = null;
    this.item2 = null;
    this.item3 = null;
    this.item4 = null;
    this.item5 = null;
    this.descricaoItem1 = '';
    this.descricaoItem2 = '';
    this.descricaoItem3 = '';
    this.descricaoItem4 = '';
    this.descricaoItem5 = '';
  }
}
