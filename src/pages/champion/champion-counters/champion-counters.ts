import { Counter } from './../../../models/Counter';
import { CountersServiceProvider } from './../../../providers/counters-service/counters-service';
import { Heroi } from './../../../models/Heroi';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ChampionCountersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-champion-counters',
  templateUrl: 'champion-counters.html',
})
export class ChampionCountersPage {
  champion: Heroi;
  heroisCounter: Heroi[];
  heroisFracos: Heroi[];

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
    private apiCounters: CountersServiceProvider) {
    console.log('Passed params', navParams.data);
    this.champion = navParams.data as Heroi;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChampionCountersPage');
    this.apiCounters.getCounter(this.champion.heroiId).subscribe(
      (res: Counter[] ) => this.heroisCounter = res,
      err => console.log(err)
    );
    this.apiCounters.getContraCounter(this.champion.heroiId).subscribe(
      (res: Counter[] ) => this.heroisFracos = res,
      err => console.log(err)
    );
  }

}
