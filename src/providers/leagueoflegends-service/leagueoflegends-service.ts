import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';

import { LeagueOfLegends } from './../../models/LeagueOfLegends';
import { GlobalVariable } from './../../global';
/*
  Generated class for the LeagueoflegendsServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LeagueoflegendsServiceProvider {

  constructor(public http: Http) {
    console.log('Hello LeagueoflegendsServiceProvider Provider');
  }

  getLeagueOfLegends(): Observable<LeagueOfLegends[]> {
    return this.http.get(GlobalVariable.BASE_API + "LeagueOfLegends")
      .map(response => response.json())
      .catch((error: any) => Observable.throw(error || 'Server error')
      );
  }

  getLeagueOfLegend(id: number): Observable<LeagueOfLegends> {
    return this.http.get(GlobalVariable.BASE_API + "LeagueOfLegends/" + id)
      .map(response => response.json() as LeagueOfLegends)
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  addLeagueOfLegend(lol: LeagueOfLegends): Observable<LeagueOfLegends> {
    let lolJson = JSON.stringify(lol,
      ['nick',
        'regiaoId',
        'heroiPrincipalId',
        'heroiSecundarioId',
        'lanePrincipalId',
        'laneSecundariaId'
      ]
    );
    console.log(lolJson);
    let header = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http.post(GlobalVariable.BASE_API + "LeagueOfLegends", lolJson, { headers: header })
      .map(response => response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error')
      );
  }
}
